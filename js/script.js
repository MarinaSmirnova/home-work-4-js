function takeArgument(text) { 
    let value;
    let defaultValue = undefined;

    do {
        value = prompt(text, defaultValue);
        defaultValue = value;
        value = Number(value);

    } while (isNaN(value));

    return value;
}

let argumentOne = takeArgument("Write the first number");
let argumentTwo = takeArgument("Write the second number");
let mathOperator = prompt("Enter math operator (+ - * /)");


function calculation(x, y, operotor) {
    let calc;
    
    switch (operotor) {
        case "+":
            calc = x + y;
            break;

        case "-":
            calc = x - y;
            break;

        case "*":
            calc = x * y;
            break;

        case "/":
            calc = x / y;
            break;
        
        default:
            calc = undefined;
    }

    return calc;
}

let result = calculation(argumentOne, argumentTwo, mathOperator);

if (result != undefined) {
    console.log(result);
} else {
    console.log("no operator");
}






